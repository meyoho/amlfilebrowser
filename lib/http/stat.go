package http

import (
	"net/http"
	fb "github.com/filebrowser/filebrowser/lib"
)

func statHandler(c *fb.Context, w http.ResponseWriter, r *http.Request) (int, error) {
	r.URL.Path = sanitizeURL(r.URL.Path)
	switch r.Method {
	case http.MethodGet:
		return statGetHandler(c, w, r)
	case http.MethodOptions:
		return corsHandler(c, w, r)
	}

	return http.StatusNotImplemented, nil
}

type FileSize struct {
	// Indicates the Kind of view on the front-end (Listing, editor or preview).
	Size int64 `json:"size"`
}

func statGetHandler(c *fb.Context, w http.ResponseWriter, r *http.Request) (int, error) {
	// Gets the information of the directory/file.
	size, err := fb.StatFileSize(r.URL.Path, c.User)
	if err != nil {
		return ErrorToHTTP(err, false), err
	}

	return renderJSON(w, FileSize{Size: size})
}
