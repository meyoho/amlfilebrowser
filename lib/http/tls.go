package http

import (
	"net/http"
	fb "github.com/filebrowser/filebrowser/lib"
)

// downloadHandler creates an archive in one of the supported formats (zip, tar,
// tar.gz or tar.bz2) and sends it to be downloaded.
type TLSMessage struct {
	// Indicates the Kind of view on the front-end (Listing, editor or preview).
	Message string `json:"message"`
}

func tlsValidateHandler(c *fb.Context, w http.ResponseWriter, r *http.Request) (int, error) {
	tlsMessage := TLSMessage{"tls auth success, back to continue "}

	return renderJSON(w, tlsMessage)
}
