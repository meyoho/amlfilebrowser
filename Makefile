IMG := index.alauda.cn/alaudaorg/amlfilebrowser
TAG := v0.1.0

##################
define helpdoc
@echo
@echo -e "TAG : 生成镜像TAG"

@echo
endef
##################

docker_build:
	build/build.sh
	docker build -f Dockerfile_aml . -t ${IMG}:${TAG}

docker_push:
	docker push  ${IMG}:${TAG}

help:
	${helpdoc}
