#!/bin/sh

set -e

cd $(dirname $0)/..

# Clean the dist folder and build the assets
cd frontend
if [ -d "dist" ]; then
  rm -rf dist/*
fi;
yarn install
yarn build