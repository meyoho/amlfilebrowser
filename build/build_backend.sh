#!/bin/sh

set -e

cd $(dirname $0)/..

# Install rice tool if not present
go get github.com/GeertJohan/go.rice/rice

# Embed the assets using rice
cd lib
$GOPATH/bin/rice embed-go
cd ../cli
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o filebrowser
